#pragma once

#include <atomic>
#include <string>

#include <Windows.h>

struct MeshThumbnailDll {
    static HINSTANCE Instance;
    static std::atomic<ULONG> DllReferenceCounter;
};

extern std::string ThumbnailProviderCLSID;
