#include "MeshThumbnail.h"

#include <algorithm>
#include <codecvt>

#include <vtkGraphicsFactory.h>
//#include <vtkImagingFactory.h>
#include <vtkWindowToImageFilter.h>

#include <vtkActor.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>

#include <vtkOBJReader.h>
#include <vtkPLYReader.h>
#include <vtkPolyDataReader.h>
#include <vtkSTLReader.h>
#include <vtkSphereSource.h>
#include <vtksys/SystemTools.hxx>

#include <vtkCamera.h>

#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);

vtkSmartPointer<vtkPolyData> ReadPolyData(const char *fileName) {
    vtkSmartPointer<vtkPolyData> polyData;
    std::string extension = vtksys::SystemTools::GetFilenameExtension(std::string(fileName));
    if (extension == ".ply") {
        vtkNew<vtkPLYReader> reader;
        reader->SetFileName(fileName);
        reader->Update();
        polyData = reader->GetOutput();
    } else if (extension == ".obj") {
        vtkNew<vtkOBJReader> reader;
        reader->SetFileName(fileName);
        reader->Update();
        polyData = reader->GetOutput();
    } else if (extension == ".stl") {
        vtkNew<vtkSTLReader> reader;
        reader->SetFileName(fileName);
        reader->Update();
        polyData = reader->GetOutput();
    } else if (extension == ".vtk") {
        vtkNew<vtkPolyDataReader> reader;
        reader->SetFileName(fileName);
        reader->Update();
        polyData = reader->GetOutput();
    } else {
        vtkNew<vtkSphereSource> source;
        source->Update();
        polyData = source->GetOutput();
    }
    return polyData;
}

bool GetThumbnailImage(IStream *stream, MeshThumbnailProvider::MeshExtension ext, UINT cx, HBITMAP &phbmp, int &channels) {
    /*STATSTG st;
    stream->Stat(&st, 0);
    MessageBoxW(0, st.pwcsName, L"Info", MB_OK);

    ue::Result<ue::Image> img = ue::Result<ue::Image>("Unsupported image format");

    // Load the image from the stream
    switch (ext) {
    case MeshThumbnailProvider::MeshExtension::OBJ: {
        IStreamExr s(stream);
        img = ue::Image::loadEXR(s);
        break;
    }
    default: { } break; }
    if (!img) {
        std::string err = THUMBNAIL_PROVIDER ".thumbnail error: " + img.error();
        OutputDebugStringA(err.c_str());
        return false;
    }
    auto pic = std::move(img).value();

  */
    return false;
}

bool GetThumbnailImage(const wchar_t *fname, MeshThumbnailProvider::MeshExtension ext, UINT cx, HBITMAP &phbmp, int &channels) {
    std::string sfname = std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(fname);
    vtkSmartPointer<vtkPolyData> pd = ReadPolyData(sfname.c_str());

    MessageBoxA(nullptr, sfname.c_str(), nullptr, MB_OK);
    MessageBoxW(nullptr, fname, L"GetThumbnailImage", MB_OK);

    // Setup offscreen rendering
    vtkSmartPointer<vtkGraphicsFactory> graphicsFactory = vtkSmartPointer<vtkGraphicsFactory>::New();
    graphicsFactory->SetOffScreenOnlyMode(1);
    graphicsFactory->SetUseMesaClasses(1);

    // Create a mapper and actor
    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputData(pd);

    vtkNew<vtkActor> actor;
    actor->SetMapper(mapper);

    // A renderer and render window
    vtkNew<vtkRenderer> renderer;
    vtkNew<vtkRenderWindow> renderWindow;
    renderWindow->SetOffScreenRendering(1);
    renderWindow->AddRenderer(renderer);

    // Add the actors to the scene
    renderer->AddActor(actor);
    renderer->SetBackground(1, 1, 1); // Background color white

    // Rotate camera
#if 0
    double center[3];
    double bounds[6];
    double vn[3];
    vtkCamera *camera = renderer->GetActiveCamera();
    camera->GetViewPlaneNormal(vn);
    renderer->ComputeVisiblePropBounds(bounds);
    center[0] = (bounds[0] + bounds[1]) / 2.0;
    center[1] = (bounds[2] + bounds[3]) / 2.0;
    center[2] = (bounds[4] + bounds[5]) / 2.0;
    double w1 = bounds[1] - bounds[0];
    double w2 = bounds[3] - bounds[2];
    double w3 = bounds[5] - bounds[4];
    double radius = w1 * w1 + w2 * w2 + w3 * w3;
    radius = (radius == 0) ? (1.0) : (radius);
    radius = sqrt(radius) * 0.5;
    double distance = radius;
    camera->SetFocalPoint(center[0], center[1], center[2]);
    camera->SetPosition(center[0] + distance * vn[0], center[1] + distance * vn[1], center[2] + distance * vn[2]);
#endif

    //renderer->ResetCamera();
    renderWindow->SetSize(cx, cx);
    renderWindow->Render();

    // Retrieve rendered image
    vtkNew<vtkWindowToImageFilter> windowToImageFilter;
    windowToImageFilter->SetInput(renderWindow);
    windowToImageFilter->Update();

    vtkImageData *img = windowToImageFilter->GetOutput();
    int xmin = img->GetExtent()[0];
    int xmax = img->GetExtent()[1];
    int ymin = img->GetExtent()[2];
    int ymax = img->GetExtent()[3];

    // Make sure we actually have data.
    if (!img->GetPointData()->GetScalars()) {
        return false;
    }
    // take into consideration the scalar type
    if (img->GetScalarType() != VTK_UNSIGNED_CHAR) {
        return false;
    }

    int bpp = img->GetNumberOfScalarComponents();
    channels = bpp;
    // Row length of x axis
    int rowLength = xmax - xmin + 1; // in bytes
    int rowAdder = (4 - ((xmax - xmin + 1) * 3) % 4) % 4;

    BITMAPINFO bmi = {sizeof(BITMAPINFOHEADER)};
    bmi.bmiHeader.biWidth = xmax - xmin + 1;
    bmi.bmiHeader.biHeight = -static_cast<LONG>(ymax - ymin + 1);
    bmi.bmiHeader.biPlanes = 1;
    bmi.bmiHeader.biBitCount = bpp * 8;
    bmi.bmiHeader.biCompression = BI_RGB;

    uint8_t *pBits;
    HBITMAP hbmp = CreateDIBSection(NULL, &bmi, DIB_RGB_COLORS, reinterpret_cast<void **>(&pBits), NULL, 0);
    if (!hbmp) {
        std::string err = THUMBNAIL_PROVIDER ".thumbnail error: "
                                             "CreateDIBSection failed";
        OutputDebugStringA(err.c_str());
        return false;
    }
    phbmp = hbmp;

    int cnt = 0;
    for (int y = ymin; y <= ymax; y++) {
        unsigned char *ptr = (unsigned char *)img->GetScalarPointer(xmin, y, 0);
        if (bpp == 1) {
            for (int i = 0; i < rowLength; i++) {
                pBits[cnt++] = (ptr[i]);
                pBits[cnt++] = (ptr[i]);
                pBits[cnt++] = (ptr[i]);
            }
        }
        if (bpp == 2) {
            for (int i = 0; i < rowLength; i++) {
                pBits[cnt++] = (ptr[i * 2]);
                pBits[cnt++] = (ptr[i * 2]);
                pBits[cnt++] = (ptr[i * 2]);
            }
        }
        if (bpp == 3) {
            for (int i = 0; i < rowLength; i++) {
                pBits[cnt++] = (ptr[i * 3 + 2]);
                pBits[cnt++] = (ptr[i * 3 + 1]);
                pBits[cnt++] = (ptr[i * 3]);
            }
        }
        if (bpp == 4) {
            for (int i = 0; i < rowLength; i++) {
                pBits[cnt++] = (ptr[i * 4 + 2]);
                pBits[cnt++] = (ptr[i * 4 + 1]);
                pBits[cnt++] = (ptr[i * 4]);
            }
        }
        for (int i = 0; i < rowAdder; i++) {
            pBits[cnt++] = ((char)0);
        }
    }

    return true;
}
