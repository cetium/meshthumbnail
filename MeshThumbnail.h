#pragma once

#include "MeshThumbnailProvider.h"

bool GetThumbnailImage(IStream *stream, MeshThumbnailProvider::MeshExtension ext, UINT cx, HBITMAP &phbmp, int &channels);
bool GetThumbnailImage(const wchar_t *fname, MeshThumbnailProvider::MeshExtension ext, UINT cx, HBITMAP &phbmp, int &channels);
