#include "ClassFactory.h"
#include "MeshThumbnailDll.h"

#include <Shlwapi.h>

ClassFactory::ClassFactory(MeshThumbnailProvider::MeshExtension ext) : _referenceCounter(1), _imageExtension(ext) {
    ++MeshThumbnailDll::DllReferenceCounter;
}

ClassFactory::~ClassFactory() {
    --MeshThumbnailDll::DllReferenceCounter;
}

// IUnknown

#pragma warning(push)
#pragma warning(disable : 4838) // conversion from 'DWORD' to 'int' requires a narrowing conversion

IFACEMETHODIMP ClassFactory::QueryInterface(REFIID riid, void **ppv) {
    static const QITAB qit[] = {
        QITABENT(ClassFactory, IClassFactory),
        {0},
    };
    return QISearch(this, qit, riid, ppv);
}

#pragma warning(pop)

IFACEMETHODIMP_(ULONG) ClassFactory::AddRef() {
    return ++_referenceCounter;
}

IFACEMETHODIMP_(ULONG) ClassFactory::Release() {
    ULONG cRef = --_referenceCounter;
    if (0 == cRef) {
        delete this;
    }
    return cRef;
}

// IClassFactory

IFACEMETHODIMP ClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv) {
    HRESULT hr = CLASS_E_NOAGGREGATION;

    //MessageBoxA(0, 0, THUMBNAIL_PROVIDER "ClassFactory::CreateInstance", MB_OK);

    // pUnkOuter is used for aggregation. We do not support it.
    if (pUnkOuter == NULL) {
        hr = E_OUTOFMEMORY;

        // Create the COM component.
        MeshThumbnailProvider *pExt = new (std::nothrow) MeshThumbnailProvider(_imageExtension);
        if (pExt) {
            // Query the specified interface.
            hr = pExt->QueryInterface(riid, ppv);
            pExt->Release();
        }
    }

    return hr;
}

IFACEMETHODIMP ClassFactory::LockServer(BOOL fLock) {
    if (fLock) {
        ++MeshThumbnailDll::DllReferenceCounter;
    } else {
        --MeshThumbnailDll::DllReferenceCounter;
    }
    return S_OK;
}
