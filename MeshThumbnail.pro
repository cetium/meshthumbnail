APPNAME = MeshThumbnailTest
APPNAME = MeshThumbnail

BUILD = app
BUILD = dll

DEFINES += USE_VTK

CONFIG += vtkstatic

include(../shared/shared.pri)

DEFINES += THUMBNAIL_PROVIDER=\\\"'$${APPNAME}'\\\"
DEFINES += USE_STREAM=0

win32: LIBS += -lShlwapi -lOle32

equals(BUILD, app) {
equals(APPNAME, MeshThumbnailTest) {
SOURCES += MeshThumbnailTest.cpp
} else {
CONFIG -= console
SOURCES += main.cpp
}
}

SOURCES += \
    MeshThumbnail.cpp \
    MeshThumbnailProvider.cpp \
    MeshThumbnailDll.cpp \
    ClassFactory.cpp \
    Registry.cpp \

HEADERS += \
    MeshThumbnail.h \
    MeshThumbnailProvider.h \
    MeshThumbnailDll.h \
    ClassFactory.h \
    Registry.h \
