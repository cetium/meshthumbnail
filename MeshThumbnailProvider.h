#pragma once

#include <atomic>
#include <string>

#include <thumbcache.h> // IThumbnailProvider

class MeshThumbnailProvider :
#if USE_STREAM
    public IInitializeWithStream,
#else
    public IInitializeWithFile,
#endif
    public IThumbnailProvider {
public:
    enum class MeshExtension { STL, PLY, OBJ, VTK };

    // IUnknown
    IFACEMETHODIMP QueryInterface(REFIID riid, void **ppv);
    IFACEMETHODIMP_(ULONG) AddRef();
    IFACEMETHODIMP_(ULONG) Release();

#if USE_STREAM
    // IInitializeWithStream
    IFACEMETHODIMP Initialize(IStream *stream, DWORD grfMode);
#else
    // IInitializeWithFile
    IFACEMETHODIMP Initialize(LPCWSTR pszFilePath, DWORD grfMode);
#endif

    // IThumbnailProvider
    IFACEMETHODIMP GetThumbnail(UINT cx, HBITMAP *phbmp, WTS_ALPHATYPE *pdwAlpha);

    MeshThumbnailProvider(MeshExtension ext);

protected:
    ~MeshThumbnailProvider();

private:
    std::atomic<ULONG> _referenceCounter;
#if USE_STREAM
    IStream *_stream;
#else
    std::wstring _fname;
#endif
    MeshExtension _imageExtension;
};
