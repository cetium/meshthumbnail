#include "MeshThumbnailProvider.h"
#include "MeshThumbnail.h"
#include "MeshThumbnailDll.h"

#include <Shlwapi.h>

MeshThumbnailProvider::MeshThumbnailProvider(MeshExtension ext)
    : _referenceCounter(1),
#if USE_STREAM
      _stream(nullptr),
#endif
      _imageExtension(ext) {
    ++MeshThumbnailDll::DllReferenceCounter;
}

MeshThumbnailProvider::~MeshThumbnailProvider() {
    --MeshThumbnailDll::DllReferenceCounter;
}

// IUnknown

#pragma warning(push)
#pragma warning(disable : 4838) // conversion from 'DWORD' to 'int' requires a narrowing conversion

IFACEMETHODIMP MeshThumbnailProvider::QueryInterface(REFIID riid, void **ppv) {
    static const QITAB qit[] = {
        QITABENT(MeshThumbnailProvider, IThumbnailProvider),
#if USE_STREAM
        QITABENT(MeshThumbnailProvider, IInitializeWithStream),
#else
        QITABENT(MeshThumbnailProvider, IInitializeWithFile),
#endif
        {0},
    };
    return QISearch(this, qit, riid, ppv);
}

#pragma warning(pop)

IFACEMETHODIMP_(ULONG) MeshThumbnailProvider::AddRef() {
    return ++_referenceCounter;
}

IFACEMETHODIMP_(ULONG) MeshThumbnailProvider::Release() {
    ULONG cRef = --_referenceCounter;
    if (0 == cRef) {
        delete this;
    }

    return cRef;
}

#if USE_STREAM
// IInitializeWithStream
IFACEMETHODIMP MeshThumbnailProvider::Initialize(IStream *stream, DWORD /*grfMode*/) {
    // A handler instance should be initialized only once in its lifetime.
    HRESULT hr = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
    if (_stream == nullptr) {
        // Take a reference to the stream if it has not been initialized yet.
        hr = stream->QueryInterface(&_stream);
    }
    return hr;
}
#else
// IInitializeWithFile
HRESULT MeshThumbnailProvider::Initialize(LPCWSTR pszFilePath, DWORD grfMode) {
    MessageBoxW(0, pszFilePath, L"MeshThumbnailProvider::Initialize", MB_OK);

    //HRESULT hr = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
    HRESULT hr = E_INVALIDARG;
    if (pszFilePath) {
        if (!_fname.empty()) {
            _fname.clear();
        }
        _fname = pszFilePath;
        hr = S_OK;
    }
    return hr;
}
#endif

// IThumbnailProvider

// Gets a thumbnail image and alpha type. The GetThumbnail is called with the
// largest desired size of the image, in pixels. Although the parameter is
// called cx, this is used as the maximum size of both the x and y dimensions.
// If the retrieved thumbnail is not square, then the longer axis is limited
// by cx and the aspect ratio of the original image respected. On exit,
// GetThumbnail provides a handle to the retrieved image. It also provides a
// value that indicates the color format of the image and whether it has
// valid alpha information.
IFACEMETHODIMP MeshThumbnailProvider::GetThumbnail(UINT cx, HBITMAP *phbmp, WTS_ALPHATYPE *pdwAlpha) {
    HRESULT hr = E_OUTOFMEMORY;

    int channels;
    HBITMAP hbmp;
#if USE_STREAM
    if (!GetThumbnailImage(_stream, _imageExtension, cx, hbmp, channels))
        return E_FAIL;
#else
    if (!GetThumbnailImage(_fname.c_str(), _imageExtension, cx, hbmp, channels))
        return E_FAIL;
#endif

    hr = S_OK;
    *phbmp = hbmp;
    *pdwAlpha = (channels > 3) ? WTSAT_ARGB : WTSAT_RGB;

    return hr;
}
