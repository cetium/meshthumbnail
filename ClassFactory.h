#pragma once

#include <Unknwn.h> // IClassFactory

#include "MeshThumbnailProvider.h"

class ClassFactory : public IClassFactory {
public:
    // IUnknown
    IFACEMETHODIMP QueryInterface(REFIID riid, void **ppv);
    IFACEMETHODIMP_(ULONG) AddRef();
    IFACEMETHODIMP_(ULONG) Release();

    // IClassFactory
    IFACEMETHODIMP CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv);
    IFACEMETHODIMP LockServer(BOOL fLock);

    ClassFactory(MeshThumbnailProvider::MeshExtension ext);

protected:
    ~ClassFactory();

private:
    std::atomic<ULONG> _referenceCounter;
    MeshThumbnailProvider::MeshExtension _imageExtension;
};
