#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <Shlwapi.h>
#include <Thumbcache.h>
#include <Unknwn.h>
#include <objbase.h>

#include <stdio.h>

// Our GUID here:
LPCOLESTR ThumbnailProviderCLSID = L"{20EEDC07-F4E2-428C-9D35-F48EBF5B4DAB}";

typedef HRESULT pfDllGetClassObjectT(REFCLSID rclsid, REFIID riid, void **ppv);

int main(int argc, char **argv) {
    GUID clsid = {0};
    IIDFromString(ThumbnailProviderCLSID, &clsid);

    if (argc < 3) {
        printf("not enough arguments: dll image\n");
        return 1;
    }
    HRESULT r;
#if USE_STREAM
    IStream *pStream = NULL;
#endif
    HMODULE dll = NULL;

    dll = LoadLibraryA(argv[1]);
    if (!dll) {
        printf("can't open DLL\n");
        return 1;
    }

    pfDllGetClassObjectT *pfDllGetClassObject = (pfDllGetClassObjectT *)GetProcAddress(dll, "DllGetClassObject");

    IClassFactory *pFactory = NULL;
    r = pfDllGetClassObject(clsid, IID_IClassFactory, (void **)&pFactory);
    if (r != S_OK) {
        printf("failed: get factory: %08x\n", r);
        return 2;
    }

#if USE_STREAM
    IInitializeWithStream *pInit;
    r = pFactory->CreateInstance(NULL, IID_IInitializeWithStream, (void **)&pInit);
#else
    IInitializeWithFile *pInit;
    r = pFactory->CreateInstance(NULL, IID_IInitializeWithFile, (void **)&pInit);
#endif
    if (r != S_OK) {
        printf("FAILED: get object\n");
        return 3;
    }
    pFactory->Release();

    IThumbnailProvider *pProvider;
    r = pInit->QueryInterface(IID_IThumbnailProvider, (void **)&pProvider);
    if (r != S_OK) {
        printf("FAILED: get provider\n");
        return 5;
    }

    wchar_t wfile[256] = {0};
    MultiByteToWideChar(CP_ACP, 0, argv[2], -1, wfile, 256);
#if USE_STREAM
    r = SHCreateStreamOnFileEx(wfile, STGM_READ, 0, FALSE, NULL, &pStream);
    if (r != S_OK || !pStream) {
        printf("can't open file\n");
        return 10;
    }

    r = pInit->Initialize(pStream, 0);
    pInit->Release();
    pStream->Release();
#else
    r = pInit->Initialize(wfile, 0);
    pInit->Release();
#endif
    if (r != S_OK) {
        printf("FAILED: init provider\n");
        return 11;
    }

    HBITMAP bmp;
    WTS_ALPHATYPE alpha;
    r = pProvider->GetThumbnail(256, &bmp, &alpha);
    BITMAP bm = {};
    int res = GetObject(bmp, sizeof(bm), &bm);
    printf("GetThumbnail: %d x %d\n", bm.bmWidth, bm.bmHeight);

    pProvider->Release();
    if (r != S_OK) {
        printf("FAILED: make thumbnail\n");
        return 12;
    }

    printf("done");
}
